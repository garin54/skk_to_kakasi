# kakasi 辞書に skk の単語を追加

## 概要

[kakasi](http://kakasi.namazu.org/index.html.ja) の変換辞書に SKK の辞書を追加する方法を説明します。

kakasi の 漢字→ひらがな 変換は読めない漢字を調べるのにとても便利ですが、デフォルトの辞書に登録されていない一部の漢字は正しいひらがなに変換しません(忘我(ぼうが)、梃子(てこ)、嫌厭(けんえん)等)。

そこで、skk の中でいちばん大きな [SKK.L](http://openlab.ring.gr.jp/skk/wiki/wiki.cgi?page=SKK%BC%AD%BD%F1#p7) 辞書の単語から元の kakasi に含まれないものを kakasi 辞書に追加し正しくひらがなに変換できる漢字を増やします。

※kakasi の辞書を増やすだけなので skk 自体は不要です

```console
# 辞書を追加する前は 忘我(ぼうが) を ぼうわれ と変換しています
% echo "忘我" | kakasi -i utf8 -JH
ぼうわれ

# 辞書を追加すると正しく ぼうが と変換されます
% echo "忘我" | kakasi -i utf8 -JH
ぼうが
```

## 環境

debian/testing bullseye (debian11) で作業をしています。
ファイルのパス等は適宜読み替えてください。

## 手順

### 準備

kakasi のインストール
(kakasi の辞書生成コマンド mkkanwa も同梱されている)

```console
$ sudo apt install kakasi
```

kakasi ソースコードの取得(kakasidict ファイルを使う)
ソースコード内の辞書の元となるテキストファイル kakasidict があります。

```console
$ cd /path/to/skk_to_kakasi
$ mkdir tmp ; cd tmp
$ wget http://kakasi.namazu.org/stable/kakasi-2.3.6.tar.gz
$ tar zxvf kakasi-2.3.6.tar.gz

# ※ kakasidict は euc-jp なので utf-8 に変換する
# 変換しておかないと後で使う uniq コマンドがうまくいかない
$ nkf -w kakasi-2.3.6/kakasidict > kakasidict.utf-8
```

SKK.L 辞書を取得
他の[辞書ファイル](http://openlab.ring.gr.jp/skk/wiki/wiki.cgi?page=SKK%BC%AD%BD%F1)を使いたい時はお好みで

```console
$ cd /path/to/skk_to_kakasi/tmp
$ wget http://openlab.jp/skk/skk/dic/SKK-JISYO.L

# SKK-JISYO.L を euc-jp から utf-8 に変換
$ nkf -w SKK-JISYO.L > SKK-JISYO.L.utf-8
```

### 辞書ファイルの作成と設置

```console
# skk_to_kakasi.rb を使って SKK フォーマットから kakasi フォーマットに変換
# SKK 辞書の場所はスクリプト内の @skk_jisyo 変数で指定
$ ../skk_to_kakasi.rb > kakasidict_skk.utf-8

# 元の kakasidic にない単語のみを抽出
$ sort kakasidict_skk.utf-8 kakasidict.utf-8 | uniq -u | nkf -e > kakasidict_skk.uniq

# 13万語ほど追加されます
$ wc -l kakasidict_skk.uniq
133327 kakasidict_skk.uniq

# 元の kakasidic と skk から抽出した単語を結合し
#  mkkanwa で kakasi の辞書ファイル(kanwadict)を生成
$ cat  kakasi-2.3.6/kakasidict kakasidict_skk.uniq > kakasidict_comp
$ mkkanwa kanwadict kakasidict_comp

# 元の kanwadict を退避
$ sudo cp /usr/share/kakasi/kanwadict /usr/share/kakasi/kanwadict.$(date +%Y%m%d)

# 新しい kanwadict を設置
$ sudo cp kanwadict /usr/share/kakasi/kanwadict
```

### 動作確認

```console
# 元の kakasi 辞書に含まれていない漢字が正しく変換できればよい
% echo "忘我" | kakasi -i utf8 -JH
ぼうが
```
