#!/usr/bin/env ruby

class SkkToKakasi
  def initialize
    # ベースとなる辞書ファイル。通常はカレントディレクトリの SKK-JISYO.L.utf-8 辞書を指定
    @skk_jisyo = %w[./SKK-JISYO.L.utf-8]
    @skk_list  = []
  end

  def run
    create_henkan_list
    output_henkan_list
  end

  def create_henkan_list
    entries.each do |yomi, henkans|
      add_to_henkan_list(yomi, henkans)
    end
  end

  def add_to_henkan_list(yomi, henkans)
    henkans.each_with_index do |henkan, i|
      henkan_without_sp = henkan.tr(' ', '_')

      # コメント, アノテーションの削除
      henkan = henkan.gsub(/\s*;.*/, "")

      @skk_list << %[#{yomi} #{henkan}]
    end
  end

  def entries
    ent = {}
    @skk_jisyo.each do |file|
      ent.merge! entries_from_file(file)
    end
    ent
  end

  # ベースファイルから yomi: henkans のハッシュを生成する
  def entries_from_file(file)
    okuri_nasi_flg = false
    File.readlines(file).map do |line|
      line = line.strip
      next if line.empty? || line.match?(/^[;#><]/)

      yomi, henkan = line.split(/\s+/, 2)
      henkans = henkan.strip.split("/").reject(&:empty?)
      [yomi.to_sym, henkans]
    end.compact.to_h
  end

  # 変換リストをファイルに出力する
  def output_henkan_list
    puts @skk_list.join("\n")
  end
end

# --- main ---
return unless $PROGRAM_NAME == __FILE__
skkc = SkkToKakasi.new
skkc.run
